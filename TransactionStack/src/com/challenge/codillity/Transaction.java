package com.challenge.codillity;

// you can also use imports, for example:
import java.util.*;

// you can use System.out.println for debugging purposes, e.g.
// System.out.println("this is a debug message");

/**
 * @since 2018-12-24
 * @author emanuelteixeira
 */
public class Transaction {

	private Transaction parent;
	private Transaction openTrans;
	private Stack<Integer> transValues;

	private int transId;
	private int counter;

	private boolean traceTransaction;

	public Transaction() {
		this.transValues = new Stack<Integer>();
	}

	/**
	 * 
	 * If has an opened transaction push to them If has no opened transaction push
	 * direct to root (auto commit)
	 * 
	 * @param value
	 *            to push to stack
	 */
	public void push(int value) {
		if (this.openTrans == null) {
			this.transValues.push(value);
		} else {
			this.openTrans.transValues.push(value);
		}

		this.traceTransaction("push()", value);
	}

	/**
	 * If has an opened transaction get topmost element from transaction stack If
	 * has no a opened transaction get topmost element from root stack
	 * 
	 * @return topmost element on stack without removing them
	 */
	public int top() {
		int topmost = this.getTopMostElement();
		this.traceTransaction("top()", null, topmost);
		return topmost;
	}

	/**
	 * If has an opened transaction get topmost element from transaction stack If
	 * has no a opened transaction get topmost element from root stack
	 * 
	 * @return topmost element on stack without removing them
	 */
	private int getTopMostElement() {

		Transaction currentTransaction = this;

		if (this.openTrans != null) {
			currentTransaction = this.openTrans;
		}

		if (!currentTransaction.transValues.isEmpty()) {
			return currentTransaction.transValues.peek();
		} else {
			return 0;
		}
	}

	/**
	 * If has an opened transaction get topmost element from transaction stack If
	 * has no a opened transaction get topmost element from root stack
	 * 
	 * Removes the element
	 */
	public void pop() {

		if (this.openTrans != null) {

			if (!this.openTrans.transValues.isEmpty()) {
				this.openTrans.transValues.pop();
			}
		} else {
			if (!this.transValues.isEmpty()) {
				this.transValues.pop();
			}
		}

		this.traceTransaction("pop()");
	}

	/**
	 * Method that starts a new transaction
	 * 
	 * Transactions has an id and a parent that correlates relationships between
	 * transactions
	 */
	public void begin() {
		Transaction transaction = new Transaction();
		transaction.transId = ++counter;

		if (this.openTrans != null) {
			transaction.parent = this.openTrans;
		} else {
			transaction.parent = this;
		}

		transaction.transValues = (Stack<Integer>) transaction.parent.transValues.clone();

		this.openTrans = transaction;

		this.traceTransaction("begin()", transaction.transId);
	}

	/**
	 * Encapsulation of rollback mechanism
	 * 
	 * @return true if has any transaction to rollback, or false if has no
	 *         transaction to rollback
	 */
	public boolean rollback() {

		int rollbackId = this.rollbackTransaction();
		boolean isRollback = (rollbackId > 0 ? true : false);

		this.traceTransaction("rollback()", rollbackId, isRollback);

		return isRollback;
	}

	/**
	 * @return transId if has any transaction to rollback, or -1 if has no
	 *         transaction to rollback
	 */
	private int rollbackTransaction() {

		if (this.openTrans == null) {
			return -1;
		} else {

			Transaction t = this.openTrans;

			if (t.parent != null && !t.parent.equals(this)) {
				this.openTrans = t.parent;
			} else {
				this.openTrans = null;
			}

			return t.transId;
		}
	}

	/**
	 * Encapsulation of commit mechanism
	 * 
	 * @return true if has any transaction to commit, or false if has no transaction
	 *         to commit
	 */
	public boolean commit() {
		int commitId = this.commitTransaction();
		boolean isCommit = (commitId > 0 ? true : false);

		this.traceTransaction("commit()", commitId, isCommit);

		return isCommit;
	}

	/**
	 * @return transId if has any transaction to commit, or -1 if has no transaction
	 *         to commit
	 */
	private int commitTransaction() {

		if (this.openTrans != null) {
			Transaction toCommit = this.openTrans;
			if (toCommit.parent != null) {
				// toCommit.currentTrans.forEach(f -> toCommit.parent.currentTrans.push(f));
				toCommit.parent.transValues = (Stack<Integer>) toCommit.transValues.clone();

				if (this != this.openTrans.parent) {
					this.openTrans = toCommit.parent;
				} else {
					this.openTrans = null;
				}
				return toCommit.transId;
			}
		}

		return -1;
	}

	public boolean isTraceTransaction() {
		return this.traceTransaction;
	}

	public void setTraceTransaction(boolean traceTransaction) {
		this.traceTransaction = traceTransaction;
	}

	/**
	 * Trace transaction from outside
	 */
	public void traceTransaction() {
		this.traceTransaction("traceTransaction()", null, null);
	}

	private void traceTransaction(String method) {
		this.traceTransaction(method, null, null);
	}

	private void traceTransaction(String method, Object value) {
		this.traceTransaction(method, value, null);
	}

	/**
	 * Prints current status of Transaction
	 * 
	 * @param method
	 *            name that perform last action
	 * @param value
	 *            (if it's a commit/rollback means the transId of affected
	 *            Transaction)
	 * @param returnValue
	 *            value that was returned in method
	 */
	private void traceTransaction(String method, Object value, Object returnValue) {

		if (!traceTransaction) {
			return;
		}

		StringBuilder trace = new StringBuilder();

		if (method != null) {
			trace.append(method).append(" ");
		}
		if (value != null) {
			trace.append(String.format("[value : %s] ", value));
		}
		if (returnValue != null) {
			trace.append(String.format("[return : %s] ", returnValue));
		}

		trace.append("-> ").append(this.toString());

		System.out.println(trace.toString());
	}

	public String toString() {

		StringBuilder builder = new StringBuilder();

		builder.append(String.format("commitedStack : %s", this.transValues));

		if (this.openTrans != null) {

			int parentId = ((this.openTrans.parent == null) ? 0 : this.openTrans.parent.transId);

			String.format(", {transId : %d, this : %s , parent[transId : %d]}", this.openTrans.transId,
					this.openTrans.transValues, parentId);
		}

		return builder.toString();
	}

	public static void main(String[] args) {

	}

	public void destroy() {
		this.parent = null;
		this.openTrans = null;
		this.transValues = null;
		this.transId = -1;
		this.counter = -1;
		this.counter = -1;
		this.traceTransaction = false;
	}

};