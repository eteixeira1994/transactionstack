package com.challenge.codillity.test;

import org.junit.Before;
import org.junit.Test;

import com.challenge.codillity.Transaction;

public class TransactionTest {
	
	
	@Before
	public void before() {
		
	}
	
	
	@Test
	public void test1A() {
		Transaction transaction = new Transaction();
		transaction.push(4);
		transaction.begin(); // start transaction 1
		transaction.push(7); // stack: [4,7]
		transaction.begin(); // start transaction 2
		transaction.push(2); // stack: [4,7,2]
		assert transaction.rollback(); // rollback transaction 2
		assert transaction.top() == 7; // stack: [4,7]
		transaction.begin(); // start transaction 3
		transaction.push(10); // stack: [4,7,10]
		assert transaction.commit(); // transaction 3 is committed
		assert transaction.top() == 10; // 10
		assert transaction.rollback(); // rollback transaction 1
		assert transaction.commit() == false; // rollback transaction 1
		assert transaction.top() == 4; // stack: [4]
		
		transaction.destroy();
	}
	
	@Test
	public void test2A() {
		
		Transaction transaction = new Transaction();
		transaction.begin();
		transaction.push(1);
		transaction.push(2);
		transaction.push(3);
		transaction.push(4);
		assert transaction.top() == 4;
		transaction.pop();
		assert transaction.top() == 3;
		assert transaction.commit();
		assert transaction.rollback() == false;
		transaction.pop();
		assert transaction.top() == 2;
		transaction.begin();
		assert transaction.commit();
		transaction.push(19);
		assert transaction.top() == 19;
		transaction.begin();
		transaction.pop();
		transaction.pop();
		transaction.pop();
		transaction.pop();
		assert transaction.top() == 0;
		assert transaction.rollback();
		assert transaction.top() == 19;
		
		transaction.destroy();
	}

	@Test
	public void test3A() {

		Transaction transaction = new Transaction();
		transaction.pop();
		assert transaction.top() == 0;
		transaction.begin();
		transaction.push(10);
		transaction.begin();
		transaction.push(10);
		transaction.begin();
		transaction.push(30);
		transaction.pop();
		transaction.pop();
		assert transaction.commit();
		transaction.push(40);
		assert transaction.commit();
		assert transaction.top() == 40;
		assert transaction.rollback();
		assert transaction.top() == 0;
		
		transaction.destroy();
	}
	
	@Test
	public void test4A() {

		Transaction transaction = new Transaction();
		int n = 10000;
		
		for (int i = 1; i <= n; i++) {
			transaction.begin();
			transaction.push(i);
		}
		
		assert  transaction.top() == n;
		
		transaction.destroy();
	}
	
	@Test
	public void test5A() {

		Transaction transaction = new Transaction();
		int n = 100000;
		
		for (int i = 1; i <= n; i++) {
			transaction.begin();
			transaction.push(i);
			assert transaction.rollback();
		}
		
		assert  transaction.top() == 0;

		transaction.destroy();
	}
	
	@Test
	public void test6A() {

		Transaction transaction = new Transaction();
		int n = 100000;

		for (int i = 0; i < 100; i++) {
			transaction.begin();
			
			for (int k = 1; k <= n; k++) {
				transaction.push(k);
			}
			
			if (i % 2 == 0) {
				transaction.commit();	
			}
			else {
				transaction.rollback();
			}
		}
		
		assert  transaction.top() == n;
		
		transaction.destroy();
	}
	
}
